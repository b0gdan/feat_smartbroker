const delay = ms => new Promise(res => setTimeout(res, ms));

// function doLogin () {
//     const button = document.createElement('button');
//     button.textContent = 'Feel pass!'
//     document.body.insertAdjacentElement('afterbegin', button);
//     button.addEventListener('click', () => {
//         console.log('Clicked');
//         document.getElementById('login').value="i@bogdan.co";
//         document.getElementById('password').value="";
//         document.forms[0].submit()
//     });
// }


async function sendAlarm () {
    var timoutSek = 1000 * 10;
    while (true) {
        console.log("Waiting " + timoutSek +" seconds....");
        await delay(timoutSek);
        chrome.runtime.sendMessage('', {
            type: 'notification',
            options: {
              title: 'Attention required!',
              message: 'Please log in one more time.',
              iconUrl: '/icons/finelbo_logo_adobe_express_128.png',
              type: 'basic'
            }
          });
    }
}

sendAlarm();