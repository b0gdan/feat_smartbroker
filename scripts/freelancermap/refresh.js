function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

const mouse_click_evt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
        clientX: 20,
    }),
element_to_click = document.getElementById("realSubmitButton");

const delay = ms => new Promise(res => setTimeout(res, ms));

async function go () {
    var currentDate = new Date();
    var timoutMin = 1000 * 60;
    console.log("Current timestamp is: " + currentDate);
    var searchText = 'Ihre Sitzung wurde beendet';
    await delay(1000 * 4); // Wait 4 seconds to get the site loaded
    if (window.find(searchText)) {
        location.reload();
    }
    var timeoutMultiplicator = getRndInteger(20, 40)
    console.log("Waiting " + timeoutMultiplicator +" minutes....");
    await delay(timoutMin * timeoutMultiplicator);
    console.log("Waited " + timeoutMultiplicator +", additionally waiting 1 minute");
    await delay(timoutMin);
    element_to_click.dispatchEvent(mouse_click_evt);
}

go();

