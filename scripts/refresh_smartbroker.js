/*const elem = document.createElement('button');
elem.textContent = 'Sample button';
elem.setAttribute('onClick', 'alert(\'Clicked\')')
elem.setAttribute('id', 'btn')
elem.style.cssText = 'position:absolute;width:200px;height:100x;';
document.body.appendChild(elem);*/

const mouse_click_evt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
        clientX: 20,
    }),
    element_to_click = document.getElementById("sessionTimer");

let iterator = 0;
function go () {
    iterator++;
    console.log(iterator + ' Loop is running, pressing the button.');
    element_to_click.dispatchEvent(mouse_click_evt);
    setTimeout(go, 2 * 60 * 1000); // callback
}
go();

